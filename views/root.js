import { LitElement } from "../vendor/lit-element.js";

import { render } from "../common/View.js";

class RootView extends LitElement{}

customElements.define( "root", SomeView );

export function root( { details = {}, el } = {} ){
    var view = render( "root", el );
}