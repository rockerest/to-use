import { commonRouteChange } from "../common/Routing.js";

export function register( router, publish ){
	router( "/", ( ctx ) => {
		let msg = commonRouteChange( ctx );

		msg.el = ctx.el;
		msg.view = "root";
        msg.ctx = ctx;

		publish( msg );
	} );
}