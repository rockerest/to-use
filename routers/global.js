import { getRootNode } from "../common/DOM.js";
import { getSearch } from "../common/URL.js";
import { getLocale } from "../common/L10n.js";

export function register( router ){
	router(
		"*",
		( ctx, next ) => {
			ctx.el = getRootNode();

			next();
		},
		( ctx, next ) => {
			ctx.search = getSearch( ctx.querystring );
			next();
		},
		( ctx, next ) => {
			ctx.locale = getLocale();

			next();
		}
	);
}