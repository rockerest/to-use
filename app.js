import { start, subscribeWith, publish } from "./common/MessageBus.js";

import { registrar as routingRegistrar } from "./actions/Routing.js";

import { message as ROUTING_INIT } from "./messages/ROUTING_INIT.js";
import { message as ROUTING_READY } from "./messages/ROUTING_READY.js";
import { message as ROUTING_START } from "./messages/ROUTING_START.js";

var partiallyAppliedSubscribe = ( map ) => subscribeWith( map, bus );
var partiallyAppliedPublish = ( message ) => publish( message, bus );
var routingReady = false;
var bus;

window.NAMESPACE = "app";
window.app = {};

bus = start();

routingRegistrar( partiallyAppliedSubscribe, partiallyAppliedPublish );

partiallyAppliedSubscribe( {
	[STORAGE_READY.name]: () => {
		storageReady = true;
		partiallyAppliedPublish( SYSTEM_CONFIGURE );
		partiallyAppliedPublish( USERS_REQUEST );
		allReady();
	},
	[ROUTING_READY.name]: () => {
		partiallyAppliedPublish( ROUTING_START );
	}
} );

partiallyAppliedPublish( ROUTING_INIT );
