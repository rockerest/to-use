import page from "page/page.mjs";

import { register as registerGlobalRoutes } from "../routers/global.js";
import { register as registerRootRoutes } from "../routers/root.js";

import { root } from "../views/root.js";

import { message as ROUTING_INIT } from "../messages/ROUTING_INIT.js";
import { message as ROUTING_READY } from "../messages/ROUTING_READY.js";
import { message as ROUTING_START } from "../messages/ROUTING_START.js";
import { message as ROUTING_CHANGE } from "../messages/ROUTING_CHANGE.js";

var views = {
	root
};

export function registrar( subscribeWith, publish ){
	subscribeWith( {
		[ROUTING_INIT.name]: () => {
			let router = page;

			window[ window.NAMESPACE ].router = router;

			if( window.runtime ){
				router.base( "/some-base" );
				delete window.runtime;
			}

			registerGlobalRoutes( router, publish );
			registerRootRoutes( router, publish );

			publish( ROUTING_READY );
		},
		[ROUTING_START.name]: () => {
			window[ window.NAMESPACE ].router();
		},
		[ROUTING_CHANGE.name]: ( message ) => {
			let el = message.el || document.body;

			if( views[ message.view ] ){
				views[ message.view ]( {
					"context": message.context,
					"details": message.details,
					el
				} );
			}
		}
	} );
}
