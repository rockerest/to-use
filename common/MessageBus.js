import { Subject } from "rxjs/_esm2015/index.js";

import { subscribeWith as internalSubscribeWith } from "./Observable.js";

export function start( global = window ){
	var bus = new Subject();

	if( global && global.NAMESPACE ){
		global[ global.NAMESPACE ].bus = bus;
	}

	return bus;
}

export function get( global = window ){
	return global[ global.NAMESPACE ].bus;
}

export function publish( msg, bus = get() ){
	return bus.next( msg );
}

export function subscribeWith( map = {}, bus = get() ){
	return bus.subscribe( internalSubscribeWith( map ) );
}

export function once( message, action, bus = get() ){
	var stream;
	var map = {
		[message]: ( ...args ) => {
			action( ...args );

			stream.unsubscribe();
		}
	};

	stream = subscribeWith( map, bus );

	return stream;
}
