import { message as ROUTING_CHANGE } from "../messages/ROUTING_CHANGE.js";

export function commonRouteChange( ctx ){
	return Object.assign( {}, ROUTING_CHANGE, {
		"context": ctx,
		"el": ctx.el
	} );
}
