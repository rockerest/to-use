export function render( name, el = document.body ){
	var renderedNow = el.renderedNow;
	var view;

	if( renderedNow == name ){
		view = el.querySelector( name );
	}
	else{
		view = document.createElement( name );

		el.innerHTML = "";
		el.appendChild( view );
		el.renderedNow = name;
	}

	return view;
}