function getFirstLanguage( navigator ){
	return ( navigator.languages && navigator.languages[ 0 ] ) || navigator.language;
}

function resolveLanguages( navigator ){
	return getFirstLanguage( navigator ) || navigator.userLanguage;
}

function getFallbackLocale(){
	return typeof navigator === "undefined" ? "en-US" : resolveLanguages( navigator );
}

export function getLocale(){
	return localStorage.getItem( "locale" ) || getFallbackLocale() || "en-US";
}